// 27/05/2018 by Dmitry Savchenko

import UIKit
import Alamofire

class ViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!

	override func viewDidLoad() {
		super.viewDidLoad()

		Alamofire
			.request("http://ocome.uppermind.ru/public/")
			.responseJSON { data in

			if let result = data.result.value as? Dictionary<String, String> {

				// Еще лаконичнее работать с Codable
				let model = Model(data: result["data"]!)
				print(model.data)
				
			}
		}
	}
}

// обработка событий
extension ViewController: UITableViewDelegate {

	// Обработка нажатия на ячейку таблицы
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print(indexPath.row)
	}
}

// подгрузка данных в таблицу
extension ViewController: UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 20
	}

	// заполняем ячейки
	func tableView(_ tableView: UITableView,
				   cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		let cell = UITableViewCell()
		cell.textLabel?.text = "\(indexPath.row)"

		return cell
	}
}

